# dcc603_software_engineering

## Authors/Autores:  
https://github.com/ananigri  
https://github.com/ArturHFS  
https://github.com/bernardosladeira  
Eduardo  
https://github.com/joaomonteiro234  
https://gitlab.com/laura-vianna  
https://github.com/mricardoferreira  
https://github.com/rodriguesbreno  

### Repository for group coursework of Software Engineering - 2018/1  

1. Presentation 1 - Basic RenPy project Setup - Python2.7 and RenPy
2. Presentation 2 - Game prototype - Python2.7 and RenPy
3. Presentation 3 - Random events and more functionalities - Python2.7 and RenPy
4. Presentation 4 - Automated testing and Final details implemented - Python2.7 and RenPy

### Repositorio para trabalho em grupo da disciplina Engenharia de Software - 2018/1  
1. Apresentacao 1 - Configuracao inicial do projeto RenPy - Python2.7 and RenPy
2. Apresentacao 2 - Prototipo do jogo - Python2.7 and RenPy
3. Apresentacao 3 - Eventos aleatorios e mais funcionalidades - Python2.7 and RenPy
4. Apresentacao 4 - Testes automatizados e detalhes finais implementados - Python2.7 and RenPy
