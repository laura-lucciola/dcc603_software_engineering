# The init python statement runs Python at initialization time, before the game loads.
init python:
    # This file has the Sprint Result Python Functions
    import python_files.sprint_result as pyr

###############################################################################
# Sprint Result Renpy Program
###############################################################################

label sprint_result:

    scene sprint_result
    show businessman
    stop music

    python:
        completion_percent = pyr.calculate_completion_percent(completed_this_sprint,not_completed_this_sprint)
        str_completed = ", ".join([x.category for x in completed_this_sprint])
        str_not_completed = ", ".join([x.category for x in not_completed_this_sprint])

    c "Parabéns [player_name], você concluiu uma etapa. Veja como foi seu progresso na corrida"

    if completion_percent != 100:
        play sound "sounds/sprint_result_ok.ogg" fadeout 1.0 fadein 1.0

        c "Infelizmente, algumas tarefas não foram concluídas e a corrida foi abortada"
        c "Não desanime! Lembre-se que o processo de aprendizado é incremental"

        if completion_percent != 0:
            c "Você atingiu [completion_percent]\% da corrida, concluindo os seguintes requisitos:"
            c  "[str_completed]"

        c "Infelizmente, você não concluiu estes requisitos:"
        c "[str_not_completed]"
        c "Como eu sou o mestre do Scrum, você quer a minha ajuda para resolver os requisitos que faltaram?"
        play music "sounds/Abackground.ogg" fadeout 1.0 fadein 1.0

        menu:
            "Sim, quero aprender mais!":
                jump learn_more

            "Não, eu consigo resolver sozinho":
                jump next_step

    else:
        play sound "sounds/sprint_result_good.ogg" fadeout 1.0 fadein 1.0

        c "Parabéns! Você atingiu o objetivo da corrida, concluindo os seguintes requisitos:"
        c "[str_completed]"

        c "Você está aprendendo como coordenar a equipe de desenvolvedores para desenvolver o jogo [project_name] \
          através da utilização do método Scrum. Continue assim, tenho certeza que conseguirá!"

        jump next_step

label learn_more:
    scene sprint_result_learn_more
    show businessman

    c "Parece que algumas tarefas não foram completadas, sugiro que na próxima corrida:"
    c "Altere as prioridades das tarefas à fazer"
    c "Coloque as tarefas com menos peso junto as de maior peso em uma iteração"
    c "Retire algumas tarefas com peso maior da categoria 'Á fazer'"

    hide businessman

    jump next_step

label next_step:
    scene sprint_result

    c "Ótimo, vamos para a próxima iteração!"

    hide businessman

    call screen to_do
