# The init python statement runs Python at initialization time, before the game loads.
init python:
    # This file has the Random Events Python Functions
    import python_files.random_events as pyre
    import constants

###############################################################################
# Random Events Renpy Program
###############################################################################

label heavier_task:
    scene heavier_task
    stop music
    play sound "sounds/bad_event.ogg" fadeout 1.0 fadein 1.0

    $ pyre.make_user_story_heavier(bl_sprint, constants.USER_STORY_POINTS)

    "Seus desenvolvedores pesaram uma atividade errada, seu peso real é bem maior! Você provavelmente não vai conseguir concluir todas as atividades planejadas nessa iteração..."

    $ compute_sprint_result()

label lighter_task:
    scene lighter_task
    stop music
    play sound "sounds/good_event.ogg" fadeout 1.0 fadein 1.0

    $ pyre.make_user_story_lighter(bl_sprint, constants.USER_STORY_POINTS)

    "Seus desenvolvedores pesaram uma atividade errada, mas seu peso real é bem menor! Você irá ter mais facilidade concluindo atividades nesta iteração!"

    $ compute_sprint_result()


label project_scope_increased:
    scene project_scope_increased
    stop music
    play sound "sounds/bad_event.ogg" fadeout 1.0 fadein 1.0

    $ pyre.increase_project_scope(bl_todo, constants.USER_STORY_POINTS)

    "Seu cliente mudou o escopo de uma das funcionalidades do seu programa, você terá que adaptar suas atividades para se adequar às mudanças!"

    $ compute_sprint_result()


label project_scope_reduced:
    scene project_scope_reduced
    stop music
    play sound "sounds/good_event.ogg" fadeout 1.0 fadein 1.0

    $ pyre.reduce_project_scope(bl_todo, bl_sprint, bl_done)

    "O cliente decidiu remover algumas funcionalidades, que sorte! Seu projeto agora tem menos atividades!"

    $ compute_sprint_result()

label new_bug:
    scene new_bug
    stop music
    play sound "sounds/surprise_event.ogg" fadeout 1.0 fadein 1.0

    $ pyre.add_new_bug(bl_todo)

    "Oh não! Um dos seus desenvolvedores realizou o envio de código com defeito e acabou introduzindo um bug no programa! Ele precisa ser corrigido"

    $ compute_sprint_result()

label sick_dev:
    scene sick_dev
    stop music
    play sound "sounds/surprise_event.ogg" fadeout 1.0 fadein 1.0

    "Oh não! Um de seus desenvolvedores adoeceu e terá licença médica durante toda a corrida! Sua velocidade irá cair por causa disso…"

    $ compute_sprint_result(sick_dev=True)
