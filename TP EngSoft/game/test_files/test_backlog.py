###############################################################################
# Backlog Tests
###############################################################################
import unittest
import sys
sys.path.insert(0, './game/')
import python_files.backlog as pyb
import constants as ct
import user_story_class as us


class TestAddToSprint(unittest.TestCase):
    def test_with_empty_param(self):
        bl_src = []
        values = []
        bl_sprint = []
        pyb.add_to_sprint(bl_src, values, bl_sprint)
        self.assertEqual(bl_sprint, [])
        self.assertEqual(bl_src, [])
        self.assertEqual(values, [])

    def test_with_empty_source(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        bl_src = []
        values = [us2]
        bl_sprint = [us1]
        pyb.add_to_sprint(bl_src, values, bl_sprint)
        self.assertEqual(bl_sprint, [us1,us2])
        self.assertEqual(bl_src, [])
        self.assertEqual(values, [])

    def test_with_empty_values(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        bl_src = [us1]
        values = []
        bl_sprint = [us2]
        pyb.add_to_sprint(bl_src, values, bl_sprint)
        self.assertEqual(bl_sprint, [us2])
        self.assertEqual(bl_src, [us1])
        self.assertEqual(values, [])

    def test_with_empty_sprint(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        bl_src = [us1, us2]
        values = [us1]
        bl_sprint = []
        pyb.add_to_sprint(bl_src, values, bl_sprint)
        self.assertEqual(bl_sprint, [us1])
        self.assertEqual(bl_src, [us2])
        self.assertEqual(values, [])

    def test_with_valid_param(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        bl_src = [us1, us2]
        values = [us1]
        bl_sprint = [us3]
        pyb.add_to_sprint(bl_src, values, bl_sprint)
        self.assertEqual(bl_sprint, [us3, us1])
        self.assertEqual(bl_src, [us2])
        self.assertEqual(values, [])

class TestRemoveFromSprint(unittest.TestCase):
    def test_with_empty_param(self):
        selected_us = None
        bl_sprint = []
        bl_todo = []
        bl_in_progress = []
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [])
        self.assertEqual(bl_todo, [])
        self.assertEqual(bl_in_progress, [])

    def test_with_empty_selected_us(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        selected_us = None
        bl_sprint = [us1]
        bl_todo = [us2]
        bl_in_progress = [us3]
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [us1])
        self.assertEqual(bl_todo, [us2])
        self.assertEqual(bl_in_progress, [us3])

    def test_with_empty_bl_sprint(self):
        us2 = us.UserStory("Story2", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        selected_us = 0
        bl_sprint = []
        bl_todo = [us2]
        bl_in_progress = [us3]
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [])
        self.assertEqual(bl_todo, [us2])
        self.assertEqual(bl_in_progress, [us3])

    def test_with_empty_bl_todo(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        selected_us = 0
        bl_sprint = [us1]
        bl_todo = []
        bl_in_progress = [us3]
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [us1])
        self.assertEqual(bl_todo, [])
        self.assertEqual(bl_in_progress, [us3])

    def test_with_empty_bl_todo(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        selected_us = 0
        bl_sprint = [us1]
        bl_todo = [us3]
        bl_in_progress = []
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [us1])
        self.assertEqual(bl_todo, [us3])
        self.assertEqual(bl_in_progress, [])

    def test_with_valid_param(self):
        us1 = us.UserStory("Story1", "Description", 5)
        us2 = us.UserStory("Story2", "Description", 5)
        us3 = us.UserStory("Story3", "Description", 5)
        us4 = us.UserStory("Story4", "Description", 5)
        selected_us = 1
        bl_sprint = [us1,us2]
        bl_todo = [us2, us3]
        bl_in_progress = [us3, us4]
        pyb.remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress)
        self.assertEqual(bl_sprint, [us1])
        self.assertEqual(bl_todo, [us2, us3, us2])
        self.assertEqual(bl_in_progress, [us3, us4])

class TestGenerateUserStories(unittest.TestCase):
    def test_with_size_of_list(self):
        self.assertEqual(len(pyb.generate_user_stories()), 13)

class TestComputeStoryPoints(unittest.TestCase):
    def test_with_empty_param(self):
        story_list = []
        pyb.compute_story_points(story_list)
        self.assertEqual(story_list, [])

    def test_with_valid_param(self):
        us1 = us.UserStory("Story1", 5)
        us2 = us.UserStory("Story2", 13)
        story_list = [us1, us2]
        pyb.compute_story_points(story_list)
        self.assertTrue(story_list[0].points == 5)
        self.assertTrue(story_list[1].points == 13)

class TestArePointsComputed(unittest.TestCase):
    def test_with_empty_param(self):
        bl_list = []
        src = None
        self.assertFalse(pyb.are_points_computed(bl_list, src))
    
    def test_with_valid_param_false(self):
        us1 = us.UserStory("Story1", "Description", 0)
        us2 = us.UserStory("Story2", "Description", 0)
        bl_list = [us1, us2]
        src = None
        self.assertFalse(pyb.are_points_computed(bl_list, src))

    def test_with_valid_param_true(self):
        us1 = us.UserStory("Story1", 1)
        us2 = us.UserStory("Story2", 3)
        us1.compute_points()
        us2.compute_points()
        bl_list = [us1, us2]
        src = None
        self.assertTrue(pyb.are_points_computed(bl_list, src))

class TestRandomEvent(unittest.TestCase):
    N_SAMPLES = 100

    def test_with_both_empty(self):
        bl_sprint, bl_todo = [], []
        possible_events = ["none", "sick_dev", "new_bug"]

        for i in range(self.N_SAMPLES):
            self.assertIn(pyb.random_event(bl_sprint, bl_todo), possible_events)

    def test_with_sprint_empty(self):
        us1 = us.UserStory("Story1", "Description", 0)
        us2 = us.UserStory("Story2", "Description", 0)

        bl_sprint, bl_todo = [], [us1, us2]

        possible_events = ["none", "sick_dev", "new_bug", "project_scope_increased", "project_scope_reduced"]

        for i in range(self.N_SAMPLES):
            self.assertIn(pyb.random_event(bl_sprint, bl_todo), possible_events)

    def test_with_todo_empty(self):
        us1 = us.UserStory("Story1", "Description", 0)
        us2 = us.UserStory("Story2", "Description", 0)

        bl_sprint, bl_todo = [us1, us2], []

        possible_events = ["none", "sick_dev", "new_bug", "heavier_task", "lighter_task"]

        for i in range(self.N_SAMPLES):
            self.assertIn(pyb.random_event(bl_sprint, bl_todo), possible_events)

    def test_with_both(self):
        us1 = us.UserStory("Story1", "Description", 0)
        us2 = us.UserStory("Story2", "Description", 0)
        us3 = us.UserStory("Story3", "Description", 0)
        us4 = us.UserStory("Story4", "Description", 0)

        bl_sprint, bl_todo = [us1, us2], [us3, us4]

        possible_events = ["none", "sick_dev", "new_bug", "heavier_task", "lighter_task", "project_scope_increased", "project_scope_reduced"]

        for i in range(self.N_SAMPLES):
            self.assertIn(pyb.random_event(bl_sprint, bl_todo), possible_events)

if __name__ == '__main__':
    unittest.main()
