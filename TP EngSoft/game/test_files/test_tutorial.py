###############################################################################
# Tutorial Tests
###############################################################################
import unittest
import sys
sys.path.insert(0, './game/python_files/')
import tutorial as pyt

class TestChoosePlayerName(unittest.TestCase):
    def test_blank(self):
        self.assertEqual(pyt.choose_player_name(None), "Dono(a) do Produto")

    def test_valid(self):
        self.assertEqual(pyt.choose_player_name("asdf "), "asdf")

class TestChooseCompanyName(unittest.TestCase):
    def test_blank(self):
        self.assertEqual(pyt.choose_company_name(None), "Empresa RenPy")

    def test_valid(self):
        self.assertEqual(pyt.choose_company_name(" asdf"), "asdf")

class TestChooseProjectName(unittest.TestCase):
    def test_blank(self):
        self.assertEqual(pyt.choose_project_name(None), "Projeto RenPy")

    def test_valid(self):
        self.assertEqual(pyt.choose_project_name(" asdf1 "), "asdf1")

if __name__ == '__main__':
    unittest.main()
