###############################################################################
# Sprint Result Tests
###############################################################################
import unittest
import sys
sys.path.insert(0, './game/python_files/')
import sprint_result as pyr

class TestCalculateCompletionPercent(unittest.TestCase):
    def test_valid_completion(self):
        self.assertEqual(pyr.calculate_completion_percent([1,2,3], [3,4,5]), 50)

    def test_empty_first_param(self):
        self.assertEqual(pyr.calculate_completion_percent([], [3,4,5]), 0)

    def test_empty_second_param(self):
        self.assertEqual(pyr.calculate_completion_percent([3,4,5], []), 100)

    def test_blank_first_param(self):
        self.assertEqual(pyr.calculate_completion_percent(None, [3,4,5]), 0)

    def test_blank_second_param(self):
        self.assertEqual(pyr.calculate_completion_percent([3,4,5], None), 100)


if __name__ == '__main__':
    unittest.main()
