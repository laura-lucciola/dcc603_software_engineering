###############################################################################
# Random Events Tests
###############################################################################
import unittest
import sys
sys.path.insert(0, './game/')
import python_files.random_events as pyre
import constants
import user_story_class as us


class TestMakeUserStoryHeavier(unittest.TestCase):
    def test_with_empty_param(self):
        us_list = []
        pyre.make_user_story_heavier(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list, [])

    def test_with_one_invalid_param(self):
        us_list = [us.UserStory("Name", 13, description="Description")]
        us_list[0].compute_points()
        pyre.make_user_story_heavier(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 13)

    def test_with_one_valid_param(self):
        us_list = [us.UserStory("Name", 8, description="Description")]
        us_list[0].compute_points()
        pyre.make_user_story_heavier(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 13)

    def test_with_two_param(self):
        us_list = [us.UserStory("Name", 13, description="Description"), us.UserStory("Name", 2, description="Description")]
        us_list[0].compute_points()
        us_list[1].compute_points()
        pyre.make_user_story_heavier(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 13)
        self.assertGreaterEqual(us_list[1].points, 3)

class TestMakeUserStoryLighter(unittest.TestCase):
    def test_with_empty_param(self):
        us_list = []
        pyre.make_user_story_lighter(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list, [])

    def test_with_one_invalid_param(self):
        us_list = [us.UserStory("Name", 1, description="Description")]
        us_list[0].compute_points()
        pyre.make_user_story_lighter(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 1)

    def test_with_one_valid_param(self):
        us_list = [us.UserStory("Name", 2, description="Description")]
        us_list[0].compute_points()
        pyre.make_user_story_lighter(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 1)

    def test_with_two_param(self):
        us_list = [us.UserStory("Name", 1, description="Description"), us.UserStory("Name", 5, description="Description")]
        us_list[0].compute_points()
        us_list[1].compute_points()
        pyre.make_user_story_lighter(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list[0].points, 1)
        self.assertLessEqual(us_list[1].points, 3)

class TestIncreaseProjectScope(unittest.TestCase):
    def test_with_empty_param(self):
        us_list = []
        pyre.increase_project_scope(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list, [])

    def test_with_one_invalid_param(self):
        us1 = us.UserStory("Name", 0, description="Description")
        us_list = [us1]
        pyre.increase_project_scope(us_list, constants.USER_STORY_POINTS)
        self.assertEqual(us_list, [us1])

    def test_with_one_valid_param(self):
        us_list = [us.UserStory("Name", 2, description="Description")]
        us_list[0].compute_points()
        pyre.increase_project_scope(us_list, constants.USER_STORY_POINTS)
        self.assertGreaterEqual(us_list[0].points, 1)
        self.assertLessEqual(us_list[0].points, 13)

class TestReduceProjectScope(unittest.TestCase):
    def test_with_all_empty_param(self):
        us_list = []
        pyre.reduce_project_scope(us_list, [], [])
        self.assertEqual(us_list, [])

    def test_with_empty_first_param(self):
        us1 = us.UserStory("Story1", 5, description="Description")
        us2 = us.UserStory("Story2", 2, description="Description")
        todo_list = []
        sprint_list = [us1]
        done_list = [us2]
        pyre.reduce_project_scope(todo_list, sprint_list, done_list)
        self.assertEqual(todo_list, [])
        self.assertEqual(sprint_list, [us1])
        self.assertEqual(done_list, [us2])

    def test_with_one_valid_param_in_todo_list(self):
        us1 = us.UserStory("Story1", 5, description="Description")
        us2 = us.UserStory("Story2", 2, description="Description")
        todo_list = [us1]
        sprint_list = [us2]
        done_list = []
        pyre.reduce_project_scope(todo_list, sprint_list, done_list)
        self.assertEqual(todo_list, [])
        self.assertEqual(sprint_list, [us2])
        self.assertEqual(done_list, [us1])

    def test_with_one_valid_param_in_todo_and_sprint_list(self):
        us1 = us.UserStory("Story1", 5, description="Description")
        us2 = us.UserStory("Story2", 2, description="Description")
        todo_list = [us1]
        sprint_list = [us1, us2]
        done_list = []
        pyre.reduce_project_scope(todo_list, sprint_list, done_list)
        self.assertEqual(todo_list, [])
        self.assertEqual(sprint_list, [us2])
        self.assertEqual(done_list, [us1])

class TestAddNewBug(unittest.TestCase):
    def test_with_empty_param(self):
        todo_list = []
        pyre.add_new_bug(todo_list)
        self.assertEqual(len(todo_list), 1)
        self.assertEqual(todo_list[0].category, "Corrigir bug")

    def test_with_valid_param(self):
        us1 = us.UserStory("Story1", 5, description="Description")
        todo_list = [us1]
        pyre.add_new_bug(todo_list)
        self.assertEqual(len(todo_list), 2)
        self.assertEqual(todo_list[0].category, "Story1")
        self.assertEqual(todo_list[1].category, "Corrigir bug")


if __name__ == '__main__':
    unittest.main()
