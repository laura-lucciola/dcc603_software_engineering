init python:
    import constants as ct

label bad_ending:

    scene bad_ending
    show businessman
    stop music
    play sound "sounds/bad_ending.ogg" fadeout 1.0 fadein 1.0

    c "Então [player_name]... infelizmente chegamos ao final deste jogo."
    c "Nós fornecemos todo o suporte necessário para que você conseguisse concluir pelo menos [ct.MIN_COMPLETION_GOOD_ENDING]\% do projeto, mas infelizmente você não conseguiu atingir esta meta, completando apenas [overall_completion_percentage]\%"
    c "Por causa do seu não comprometimento com o corrida, a empresa [company_name] perdeu o seu cliente mais valioso e infelizmente, teremos que começar um programa de demissão de funcionários"

    you "Espera, me dê mais uma chance, tenho certeza que na próxima eu irei conseguir!"
    you "Realmente foi muito complicado para mim, percebi que meu entendimento do método não era suficiente, mas estou empenhado a aprender cada vez mais."

    c "Não podemos fazer nada quanto a isso [player_name], não temos dinheiro para recomeçar o projeto."

    hide businessman

    menu:
        "Eu tenho meios de conseguir um investimento! Gostaria de ter mais uma chance no projeto":
            jump restart_game

        "Tudo bem, reconheço que não dei meu melhor.":
            jump end_game

label restart_game:
    "Você escolheu recomeçar o jogo"

    $ renpy.full_restart(label='explanation')

label end_game:
    "FIM DE JOGO"

    $ renpy.full_restart()
