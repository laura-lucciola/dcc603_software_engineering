label good_ending:

    scene good_ending
    show businessman
    stop music
    play sound "sounds/good_ending.ogg" fadeout 1.0 fadein 1.0

    c "Parabéns [player_name] você conseguiu concluir o projeto!"

    c "Atingindo [overall_completion_percentage]\% de todos os requisitos conseguiu gerir toda a sua equipe de desenvolvedores para desenvolver o [project_name] que está sendo um sucesso."

    c "Agora você faz parte da nossa equipe [company_name]. Estamos muitos felizes que você conseguiu!"

    hide businessman

    "FIM DE JOGO"

    $ renpy.full_restart()
