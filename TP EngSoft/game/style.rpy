## Backlog navigation
style backlog_navigation_button is gui_button
style backlog_navigation_button_text is gui_button_text

style backlog_navigation_button:
    size_group "backlog_navigation"
    properties gui.button_properties("backlog_navigation_button")

style backlog_navigation_button_text:
    properties gui.button_text_properties("backlog_navigation_button")


## Backlog menu
style backlog_menu_outer_frame is empty
style backlog_menu_navigation_frame is empty
style backlog_menu_content_frame is empty
style backlog_menu_buttons_frame is empty
style backlog_menu_viewport is gui_viewport
style backlog_menu_side is gui_side
style backlog_menu_scrollbar is gui_vscrollbar

style backlog_menu_label is gui_label
style backlog_label_text is gui_label_text

style return_button is navigation_button
style return_button_text is navigation_button_text

style backlog_menu_outer_frame:
    bottom_padding 30
    top_padding 120

    background "gui/overlay/game_menu.png"

style backlog_menu_navigation_frame:
    xsize 280
    yfill True

style backlog_menu_content_frame:
    left_margin 40
    right_margin 20
    top_margin 10

style backlog_menu_buttons_frame:
    xpos 280
    xsize 440

style backlog_menu_viewport:
    xsize 920

style backlog_menu_vscrollbar:
    unscrollable gui.unscrollable

style backlog_menu_side:
    spacing 10

style backlog_menu_label:
    xpos 50
    ysize 120

style backlog_menu_label_text:
    size gui.title_text_size
    color gui.accent_color
    yalign 0.5

style return_button:
    xpos gui.navigation_xpos
    yalign 1.0
    yoffset -30

style prioritize_button:
    yalign 1.0
    xpos 330
    ypos 570

style compute_button:
    yalign 1.0
    xpos 30
    ypos 570

style move_button:
    yalign 1.0
    xpos 150
    ypos 570