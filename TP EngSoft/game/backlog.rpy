init python:
    import random
    import user_story_class as us
    import project_statistic_class as ps
    import constants as ct
    import python_files.backlog as pyb

    ### Game Functions
    ## User Stories
    def prioritize_user_story():
        """Moves a user story one position up (unless it's the first one)"""
        global bl_sprint_selected

        if bl_sprint_selected > 0:
            bl_sprint[bl_sprint_selected-1], bl_sprint[bl_sprint_selected] = bl_sprint[bl_sprint_selected], bl_sprint[bl_sprint_selected-1]
            bl_sprint_selected -= 1


    ## Sprints
    def check_if_reached_end_game():
        # Bad ending condition: the iteration number is 8 or more. And the completion percentage is less then 60%
        if (n_iteration >= ct.NUM_SPRINTS_START_ENDING and overall_completion_percentage <= ct.MAX_COMPLETION_BAD_ENDING):
            renpy.call(label="bad_ending")

            # Good ending condition: 100% completion pergentage
        elif (n_iteration >= ct.NUM_SPRINTS_START_ENDING and overall_completion_percentage >= ct.MIN_COMPLETION_GOOD_ENDING):
                renpy.call(label="good_ending")

        # Else, end game condition is not reached, show "end of sprint" screen instead
        else:
            renpy.call(label="sprint_result")

    def compute_sprint_result(sick_dev=False):
        # Choose a random number of completed points
        # n_completed_sp = int(round(random.gauss(sp_mean, sp_std)))
        n_completed_sp = ct.TEAM_PRODUCTIVITY_BASE + pj_stats.get_bonus_us_points()

        if sick_dev:
            n_completed_sp = int(n_completed_sp*ct.SICK_DEV_FACTOR)

        global completed_this_sprint
        global not_completed_this_sprint
        global n_iteration

        completed_this_sprint = []
        not_completed_this_sprint = []

        # Computes the total amount of story points requested in the current sprint
        total_us_points = 0
        for user_story in bl_sprint:
            total_us_points = total_us_points + user_story.points

        # Allocates completed points to stories according to their priorities
        # Moves stories in "sprint" accordingly
        aux = bl_sprint[:]
        for user_story in aux:

            # Points completed for this story
            story_completed_points = min(n_completed_sp, user_story.points - user_story.completed_points)

            # Update story
            n_completed_sp -= story_completed_points
            user_story.completed_points += story_completed_points

            # If the story has been completed, move it to "done"
            if user_story.completed_points == user_story.points:
                bl_done.append(user_story)
                completed_this_sprint.append(user_story)

            # If the user story was started it goes to "in progress"
            elif user_story.completed_points != 0:
                bl_in_progress.append(user_story)
                not_completed_this_sprint.append(user_story)

            # Otherwise it goes to "to do"
            else:
                bl_todo.append(user_story)
                not_completed_this_sprint.append(user_story)

            bl_sprint.remove(user_story)
            bl_sprint_selected = None

        global overall_completion_percentage
        overall_completion_percentage = int((len(bl_done))*100 / (len(bl_done) + len(bl_todo) + len(bl_in_progress) + len(bl_sprint)))

        pj_stats.update(bl_todo + bl_in_progress, completed_this_sprint, ct.TEAM_PRODUCTIVITY_BASE, total_us_points)

        n_iteration += 1

        check_if_reached_end_game()

    def start_sprint(bl_sprint, bl_todo):
        global n_iteration

        chosen_event = pyb.random_event(bl_sprint, bl_todo)

        if chosen_event != "none":
            renpy.call(label=chosen_event)
        else:
            compute_sprint_result()


    ## GLOBAL VARIABLES
    n_iteration = 1
    overall_completion_percentage = 0

    # Project statistic
    pj_stats = ps.ProjectStatistic(ct.CLIENT_SATISFACTION_BASE, ct.CODE_STABILITY_BASE, ct.TEAM_MOTIVATION_BASE)

    # Backlog lists
    bl_todo = pyb.generate_user_stories()
    bl_todo_selected = []

    bl_sprint = []
    bl_sprint_selected = None

    bl_in_progress = []
    bl_in_progress_selected = []

    bl_done = []
    bl_done_selected = []

    # Lists for the sprint result screen
    completed_this_sprint = []
    not_completed_this_sprint = []

    # Mean and standard deviation of how many user stories are completed
    sp_mean = ct.INIT_SP_MEAN
    sp_std = ct.INIT_SP_STD


### Screens

## Backlog Navigation
# This screen is included in the backlog menus, and provides navigation to backlog menus
screen backlog_navigation():
    style_prefix "backlog_navigation"

    vbox:
        xpos gui.navigation_xpos
        yalign 0.5

        spacing gui.navigation_spacing

        textbutton _("A Fazer") action ShowMenu("to_do")

        textbutton _("Em Progresso") action ShowMenu("in_progress")

        textbutton _("Corrida") action ShowMenu("sprint")

        textbutton _("Concluído") action ShowMenu("done")

        textbutton _("Estatísticas")  action ShowMenu("statistics")

## Backlog menu
screen backlog_menu(src, bl_list, selection, buttons=True):
    style_prefix "backlog_menu"

    add gui.game_menu_background

    frame:
        style "backlog_menu_outer_frame"

        hbox:
            ## Reserve space for the navigation section.
            frame:
                style "backlog_menu_navigation_frame"

            frame:
                style "backlog_menu_content_frame"

                viewport:
                    yinitial 0.0
                    scrollbars "vertical"
                    mousewheel True
                    draggable True
                    pagekeys True

                    side_yfill True

                    vbox:
                        transclude

        frame:
            style "backlog_menu_buttons_frame"

            if (src == "to_do") or (src == "in_progress"):
                textbutton _("Selecionar"):
                    style "move_button"

                    action Function(pyb.add_to_sprint, bl_list, selection, bl_sprint)

                    sensitive (pyb.are_points_computed(selection, bl_list))

            elif src == "sprint":
                textbutton _("Remover"):
                    style "move_button"

                    action Function(pyb.remove_from_sprint, selection, bl_sprint, bl_todo, bl_in_progress)

                    sensitive (bl_sprint_selected is not None)


            textbutton _("Orçar"):
                style "compute_button"

                action Function(pyb.compute_story_points, selection)

                if src != "to_do":
                    sensitive False

            textbutton _("Priorizar"):
                style "prioritize_button"

                action Function(prioritize_user_story)

                sensitive (src == "sprint") and (bl_sprint_selected is not None) and (bl_sprint_selected != 0)

    use backlog_navigation

    textbutton _("Começar Corrida"):
        sensitive len(bl_sprint) > 0

        style "return_button"

        action Function(start_sprint, bl_sprint, bl_todo)

    label "Reserva de Requisitos"

## Statistics menu
screen statistics_menu():
    style_prefix "backlog_menu"

    add gui.game_menu_background

    frame:
        style "backlog_menu_outer_frame"

        hbox:

            ## Reserve space for the navigation section.
            frame:
                style "backlog_menu_navigation_frame"

            frame:
                style "backlog_menu_content_frame"

                viewport:
                    yinitial 0.0
                    scrollbars "vertical"
                    mousewheel True
                    draggable True
                    pagekeys True

                    side_yfill True

                    vbox:
                        transclude

    use backlog_navigation

    textbutton _("Começar Corrida"):
        style "return_button"

        action Function(pyb.random_event, bl_sprint, bl_todo)

    label "Estatísticas do Projeto"

## Backlog screens
screen to_do():
    tag menu

    use backlog_menu("to_do", bl_todo, bl_todo_selected):
        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_todo:
                textbutton user_story.get_text():
                    action ToggleSetMembership(bl_todo_selected, user_story)

screen sprint():
    tag menu

    use backlog_menu("sprint", bl_sprint, bl_sprint_selected):
        vbox:
            spacing gui.navigation_spacing

            for i, user_story in enumerate(bl_sprint):
                textbutton "{} - {}".format(i+1, user_story.get_text()):
                    action [SetVariable("bl_sprint_selected", i)]

screen in_progress():
    tag menu

    use backlog_menu("in_progress", bl_in_progress, bl_in_progress_selected):
        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_in_progress:
                textbutton user_story.get_text():
                    action ToggleSetMembership(bl_in_progress_selected, user_story)

screen done():
    tag menu

    use backlog_menu("done", bl_done, bl_done_selected, buttons=False):
        vbox:
            spacing gui.navigation_spacing

            for user_story in bl_done:
                textbutton user_story.get_text():
                    sensitive False

screen statistics():
    tag menu

    use statistics_menu():
        vbox:
            spacing gui.navigation_spacing

            for stat_text in pj_stats.get_text_list():
                text stat_text + "\n"

            text "Produtividade do time: {} histórias por corrida".format(ct.TEAM_PRODUCTIVITY_BASE+pj_stats.get_bonus_us_points())
