###############################################################################
# Tutorial Python Functions
###############################################################################
def choose_player_name(input):
    if not input:
        return "Dono(a) do Produto"
    input = input.strip()
    return input

def choose_company_name(input):
    if not input:
        return "Empresa RenPy"
    input = input.strip()
    return input

def choose_project_name(input):
    if not input:
        return "Projeto RenPy"
    input = input.strip()
    return input
