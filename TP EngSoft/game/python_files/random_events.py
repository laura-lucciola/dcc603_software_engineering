###############################################################################
# Random Events Python Functions
###############################################################################
import random
import sys
sys.path.insert(0, './game/')
import user_story_class as us
import project_statistic_class as ps

def make_user_story_heavier(bl_sprint, story_points_list):
    possible_stories = [x for x in bl_sprint if x.points < 13]

    if len(possible_stories) > 0:
        user_story = random.choice(possible_stories)

        points_index = story_points_list.index(user_story.points)

        user_story.points = random.choice(story_points_list[points_index+1: ])

def make_user_story_lighter(bl_sprint, story_points_list):
    possible_stories = [x for x in bl_sprint if x.points > 1]

    if len(possible_stories) > 0:
        user_story = random.choice(possible_stories)

        points_index = story_points_list.index(user_story.points)

        user_story.points = random.choice(story_points_list[: points_index])


def increase_project_scope(bl_todo, story_points_list):
    possible_stories = [x for x in bl_todo if x.points != 0]

    if len(possible_stories) > 0:
        user_story = random.choice(possible_stories)
        user_story.points = random.choice(story_points_list)

def reduce_project_scope(bl_todo, bl_sprint, bl_done):
    if len(bl_todo) > 0:
        user_story = random.choice(bl_todo)

        bl_done.append(user_story)
        bl_todo.remove(user_story)

        if (len(bl_sprint) > 0) and (user_story in bl_sprint):
            bl_sprint.remove(user_story)

def add_new_bug(bl_todo):
    new_bug = us.UserStory(
        "Corrigir bug", 5, description="Bug introduzido por um dos desenvolvedores", complete_bonus={ps.CLIENT_SATISFACTION:2, ps.CODE_STABILITY:10}, delay_penalty={ps.CLIENT_SATISFACTION:5, ps.CODE_STABILITY:5}
    )
    bl_todo.append(new_bug)
