#!/usr/bin/env python
#encoding: utf-8

###############################################################################
# Backlog Python Functions
###############################################################################
import random
import sys
sys.path.insert(0, './game/')
import constants as ct
import user_story_class as us
import project_statistic_class as ps

### Game Functions
## User Stories
def add_to_sprint(bl_src, values, bl_sprint):
    if values:
        for i in values:
            if i in bl_src:
                bl_src.remove(i)
            bl_sprint.append(i)

        for i in reversed(range(len(values))):
            values.pop(i)

def remove_from_sprint(selected_us, bl_sprint, bl_todo, bl_in_progress):
    if selected_us:
        user_story = bl_sprint[selected_us]

        if user_story.completed_points == 0:
            bl_todo.append(user_story)
        else:
            bl_in_progress.append(user_story)

        bl_sprint.remove(user_story)

        selected_us = None

def generate_user_stories():
    us_list = [
        us.UserStory(b"Desenhar interface", 5, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Implementar interface", 8, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Corrigir bug", 3, complete_bonus={ps.CLIENT_SATISFACTION:2, ps.CODE_STABILITY:10}, delay_penalty={ps.CLIENT_SATISFACTION:5, ps.CODE_STABILITY:5}),
        us.UserStory(b"Implementar nova caracter\xc3\xadstica", 13, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Melhorar o desempenho de uma funcionalidade", 13, complete_bonus={ps.CLIENT_SATISFACTION:3}),
        us.UserStory(b'Otimizar c\xc3\xb3digo', 8, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Melhorar desempenho gr\xc3\xa1fico", 5, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Refatorar interface", 5, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Implementar API externa", 8),
        us.UserStory(b"Implementar IA", 13, complete_bonus={ps.CLIENT_SATISFACTION:5}),
        us.UserStory(b"Refatorar c\xc3\xb3digo", 5, complete_bonus={ps.CODE_STABILITY:10}),
        us.UserStory(b"Desenvolver Testes", 5, complete_bonus={ps.CODE_STABILITY:10}),
        us.UserStory(b"Refatorar Testes", 3, complete_bonus={ps.CODE_STABILITY:5})
    ]

    return us_list

def compute_story_points(story_list):
    for i in story_list:
        i.compute_points()

def are_points_computed(bl_list, src):
    if(len(bl_list) == 0):
        return False

    for i in bl_list:
        if(i.points == 0):
            return False

    return True


## Project statistics
def update_project_statistics(stats_list, us_left, us_done):
    for stat in stats_list:
        stat.compute_stat_value(us_left, us_done)


## Random events
def random_event(bl_sprint, bl_todo):
    """Chooses one random event"""

    events_to_use = list(ct.RANDOM_EVENTS_WEIGHT.keys())

    # These methods check if the sprint and todo backlog still have elements, so events that need it will be removed from the list
    if len(bl_sprint) == 0:
        events_to_use.remove("heavier_task")
        events_to_use.remove("lighter_task")

    if len(bl_todo) == 0:
        events_to_use.remove("project_scope_increased")
        events_to_use.remove("project_scope_reduced")

    # Return a random value from a specified categorical distribution
    weight_sum = sum([ct.RANDOM_EVENTS_WEIGHT[x] for x in events_to_use])

    random_value = random.uniform(0, weight_sum)

    cumulative_sum = 0
    for e in events_to_use:
        cumulative_sum += ct.RANDOM_EVENTS_WEIGHT[e]

        if random_value < cumulative_sum:
            return e

    return e
