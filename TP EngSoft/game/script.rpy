call image_declarations from _call_image_declarations
call character_declarations from _call_character_declarations

# The game starts here.
label start:
    play music "sounds/Abackground.ogg"
    jump tutorial
