### Game Constants
# Possible values for user story points
USER_STORY_POINTS = [1, 2, 3, 5, 8, 13]

MIN_COMPLETION_GOOD_ENDING = 85
MAX_COMPLETION_BAD_ENDING = 60
NUM_SPRINTS_START_ENDING = 6

# Project statistics base values
TEAM_PRODUCTIVITY_BASE = 30
CLIENT_SATISFACTION_BASE = 50
CODE_STABILITY_BASE = 100
TEAM_MOTIVATION_BASE = 70

# Initial values for mean and standard deviation of completed story points per sprint
INIT_SP_MEAN = 10
INIT_SP_STD = 2

# Weights used to assign probability to the events
RANDOM_EVENTS_WEIGHT =  {"none":51, "heavier_task":14, "lighter_task":7,
                         "project_scope_increased": 7, "project_scope_reduced":7,
                         "new_bug":7, "sick_dev":7}

SICK_DEV_FACTOR = 0.8